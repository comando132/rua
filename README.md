# Guía para uso de GIT
## Requisitos y guías 
- [ ] [Instalar git en su computadora](https://git-scm.com/downloads)
- [ ] [Tener una cuenta en GitLab](https://gitlab.com)
- [ ] [Guía oficial](https://git-scm.com/book/es/v2)
- [ ] [Guía para comenzar con git](https://rogerdudler.github.io/git-guide/index.es.html)

## Que es Git
[Fundamentos de Git](https://book.git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git) 

### Zonas de Git

Recordemos las 3 zonas que utiliza git:
* Directorio de trabajo 
* Área de preparado
* Repositorio

> El directorio de trabajo: Será el directorio donde hayamos creado el repositorio, y en el tendremos los archivos que se quieran añadir al proyecto en algún momento determinado. Estos archivos aún no serán tenidos en cuenta por GIT para controlar sus cambios.
>
>Staging index: O área de preparación, es un área temporal de GIT donde se guardarán los archivos que están a punto de ser enviados al repositorio.
>
> El repositorio: También llamado HEAD, será la zona donde los archivos del proyecto estarán almacenados, ordenados y controlados para ser recuperados en cualquier momento.


### Estado de los archivos

> Recuerda que cada archivo de tu repositorio puede tener dos estados: 
> rastreados y sin rastrear. 
> 
> Los archivos rastreados (tracked files en inglés) son todos aquellos  
> archivos que estaban en la última instantánea del proyecto; pueden ser 
> archivos sin modificar, modificados o preparados. 
> 
> Los archivos sin rastrear 
> son todos los demás - cualquier otro archivo en tu directorio de trabajo 
> que no estaba en tu última instantánea y que no está en el área de 
> preparación (staging area). 
> 
> Cuando clonas por primera vez un repositorio, 
> todos tus archivos estarán rastreados y sin modificar pues acabas de 
> sacarlos y aun no han sido editados.

## Trabajando con GIT

### Revisar la instalación 

En una terminal ejecutar
para verificar que git este instalado.
```sh
git --version
```

### Configuración global de Git
Lo primero que deberás hacer cuando instales Git es establecer tu nombre de usuario y dirección de correo electrónico. Esto es importante porque los "commits" de Git usan esta información, y es introducida de manera inmutable en los commits que envías:
```bash
git config --global user.name "<nombre>"
git config --global user.email "<correo>"
```

Verificar que se guardaron los datos
```bash
git config --global --list
```

### Creacion de un nuevo repositorio. 
Para crear un nuevo repositorio 

Descargar el proyecto de un repositorio remoto y subir nuevos archivos:
```bash
git clone git@gitlab.com:comando132/rua.git
cd rua
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

Subir una carpeta existente a un repositorio vacio:
```bash
cd <folder>
git init --initial-branch=main
git remote add origin git@gitlab.com:comando132/rua.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Comprobar el estatus de los archivos 
Para comprobar el estado de nuestros archivos utilizamos:


```bash
git status
git diff
```

El modo de trabajo a utilizar en git es el siguiente: 

Crearemos y modificaremos los archivos que tenemos en el directorio de trabajo, posteriormente pasaremos al staging index aquellos archivos del directorio de trabajo que queramos controlar con GIT, y después confirmaremos los cambios pasando los archivos del staging index al repositorio de GIT, para así ser controlados definitivamente.

### Agregar cambios
Para agregar los cambios a nuestra area de preparado (staging index) se puede utilizar cualquiera de estos comandos:
```bash
git add <filename>
git add .
git add -A
```

### Confirmar cambios
```bash
git commit -m "mensaje descriptivo de los cambios realizados"
```

### historial de commits.

```bash
git log
git log --oneline
git log --oneline --all --graph --decorate
```





